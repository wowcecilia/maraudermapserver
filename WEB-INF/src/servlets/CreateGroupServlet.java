package servlets;
import java.security.SecureRandom;
import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;

public class CreateGroupServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static private final String USER = "root";
  static private final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  
  @JsonInclude(Include.NON_NULL)
  static class Response {
    public enum Status {OK, FAILED, ILLEGAL_INPUT, EXPIRED};
    public Status status;
    public String groupid;
    public Response setStatus(Status s) {
      this.status = s;
      return this;
    }
    public Response setGroupid(String id) {
      this.groupid = id;
      return this;
    }
  }
  
  /**
   * http://localhost:8080/alpha/create-group?name=giant&user=4128887335&members=4128887335&valid=3
   */

  @Override
  // if LoggedIn
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    String groupName = request.getParameter("name");  
    String members = request.getParameter("members");
    String user = request.getParameter("user");
    String valid = request.getParameter("valid");
    
    PrintWriter out = response.getWriter();
    Connection conn = null;
    Statement stmt = null;
    try {
      Class.forName("com.mysql.jdbc.Driver");

//      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);
      stmt = conn.createStatement();
      String sql;
      PreparedStatement ps;
      ResultSet rs;
          
      SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
      String groupId;
      boolean qualified;
      do {
        qualified = true;
//        random = new Integer(prng.nextInt());
        groupId = Integer.toHexString(prng.nextInt());
        sql = "SELECT * from groups where groupid = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, groupId);
        System.out.println(ps);
        rs = ps.executeQuery();
        if (rs.next()) {
          qualified = false;
        }
      } while (!qualified);
      
      sql = "INSERT INTO groups values (?, ?, ?, ?, NOW() + INTERVAL ? HOUR);";
      ps = conn.prepareStatement(sql);
      ps.setString(1, groupId);
      ps.setString(2, groupName);
      ps.setString(3, user);
      ps.setString(4, user); // only the current admin is in the group
      ps.setString(5, valid);
      System.out.println(ps);
      ps.executeUpdate();
      
      
      String[] ms = members.split(",");
      for (String s: ms) {
        sql = "SELECT * from info where account = ?;";
        ps = conn.prepareStatement(sql);
        ps.setString(1, s.trim());
        System.out.println(ps);
        rs = ps.executeQuery();
        if (rs.next()) {
          sql = "INSERT INTO user_group_privacy (account, groupid, status) VALUES(?, ?, 1);";
          ps = conn.prepareStatement(sql);
          ps.setString(1, s.trim());
          ps.setString(2, groupId);
          System.out.println(ps);
          ps.executeUpdate();
        }
      }
      
      sql = "INSERT user_group_privacy(account, groupid, status) values (?, ?, 101) ON DUPLICATE KEY UPDATE status = 101;";
      ps = conn.prepareStatement(sql);
      ps.setString(1, user);
      ps.setString(2, groupId);
      System.out.println(ps);
      ps.executeUpdate();
      
      ObjectMapper mapper = new ObjectMapper();
      Response res = new Response().setStatus(Response.Status.OK).setGroupid(groupId);
      out.println(mapper.writeValueAsString(res));
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }

}// end servlet
