package servlets;
import java.sql.*;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.LeaveGroupServlet.Response.Status;
import util.IdentifierGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;

public class LeaveGroupServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static private final String USER = "root";
  static private final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  
  @JsonInclude(Include.NON_NULL)
  static class Response {
    public enum Status {OK, EXPIRED, NO_SUCH_GROUP, JOINED, BLCOKED, INVITED};
    public Status status;
    public Response setStatus(Status s) {
      this.status = s;
      return this;
    }
  }

  @Override
  // if LoggedIn
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    String account = request.getParameter("account");
    String groupid = request.getParameter("groupid");
    String block = request.getParameter("block");
    PrintWriter out = response.getWriter();
    Connection conn = null;
    Statement stmt = null;
    try {
      Class.forName("com.mysql.jdbc.Driver");

//      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);
      stmt = conn.createStatement();
      
      String sql;
      PreparedStatement ps;
      ResultSet rs;
      Response res = new Response();
      
      sql = "select * from groups where groupid = ? and expiration > now();";
      ps = conn.prepareStatement(sql);
      ps.setString(1, groupid);
      System.out.println(ps);
      rs = ps.executeQuery();
      if (!rs.next()) {
        res.setStatus(Status.NO_SUCH_GROUP);
      } else {
        sql = "select coalesce(status, 0) from user_group_privacy where account = ? and groupid = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, account);
        ps.setString(2, groupid);
        System.out.println(ps);
        rs = ps.executeQuery();
        rs.first();
        int status = rs.getInt(1);
        if (status >= 100) {
          res.setStatus(Status.JOINED);
        } else if (status < 0) {
          res.setStatus(Status.BLCOKED);
        } else if (status == 1) { // invited
          res.setStatus(Status.INVITED);
          // JOIN IT 
          sql = "INSERT INTO user_group_privacy (account, groupid, status) VALUES(?, ?, 100) ON DUPLICATE KEY UPDATE status = 100";
          ps = conn.prepareStatement(sql);
          ps.setString(1, account);
          ps.setString(2, groupid);
          System.out.println(ps);
          ps.executeUpdate();
        } else if (status == 2) { // requested before
          // do nothing?
          res.setStatus(Status.OK);
        } else {
          res.setStatus(Status.OK);
        }
      }
      
      ObjectMapper mapper = new ObjectMapper();
      out.println(mapper.writeValueAsString(res));
      rs.close();
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }

}// end servlet
