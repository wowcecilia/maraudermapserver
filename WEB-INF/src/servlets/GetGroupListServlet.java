package servlets;
import java.sql.*;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.LoginServlet.Response;
import servlets.LoginServlet.Response.Status;
import util.IdentifierGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;

public class GetGroupListServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static private final String USER = "root";
  static private final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  
  @JsonInclude(Include.NON_NULL)
  static class Response {
    public enum Status {OK, EXPIRED};
    public Status status;
    public List<String> groupid;
    public List<String> groupname;
    public List<Integer> relations;
    public Response setStatus(Status s) {
      this.status = s;
      return this;
    }
    
    public Response setGroupid(List<String> a) {
      this.groupid = a;
      return this;
    }
    
    public Response setGroupname(List<String> a) {
      this.groupname = a;
      return this;
    }
    
    public Response setRelations(List<Integer> a) {
      this.relations = a;
      return this;
    }
  }
  

  @Override
  // if LoggedIn
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    String account = request.getParameter("account");
    PrintWriter out = response.getWriter();
    Connection conn = null;
    Statement stmt = null;
    try {
      Class.forName("com.mysql.jdbc.Driver");

//      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);

      stmt = conn.createStatement();
      String sql;
      sql = "select b.groupid as groupid, b.name as name, a.status as relation from"
           + " (select * from user_group_privacy where account = ? and status > 0) a"
           + " inner join groups b using (groupid)"
           + " where expiration > Now()";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, account);
      System.out.println(ps);

      ObjectMapper mapper = new ObjectMapper();
      
      ResultSet rs = ps.executeQuery();
      ArrayList<String> ids = new ArrayList<String>();
      ArrayList<String> names = new ArrayList<String>();
      ArrayList<Integer> relations = new ArrayList<Integer>();
      while (rs.next()) {
        ids.add(rs.getString("groupid"));
        names.add(rs.getString("name"));
        relations.add(rs.getInt("relation"));
      }
      
      Response res = new Response()
        .setGroupid(ids)
        .setGroupname(names)
        .setRelations(relations)
        .setStatus(Response.Status.OK);
      out.println(mapper.writeValueAsString(res));
      rs.close();
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }

}// end servlet
