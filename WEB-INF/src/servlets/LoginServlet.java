package servlets;
import java.sql.*;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.IdentifierGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;

public class LoginServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static private final String USER = "root";
  static private final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  
  @JsonInclude(Include.NON_NULL)
  static class Response {
    public enum Status {OK, WRONG_PASS, NO_USER};
    public Status status;
    public String account;
    public String sessionid;
    public List<Map<String, String>> invites;
    public List<Map<String, String>> accepts;
    public List<Map<String, String>> rejects;
    
    public Response setStatus(Status s) {
      this.status = s;
      return this;
    }
    
    public Response setAccount(String a) {
      this.account = a;
      return this;
    }
    
    public Response setSessionid(String s) {
      this.sessionid = s;
      return this;
    }
    
    public Response setInvites(List<Map<String, String>> invites) {
      this.invites = invites;
      return this;
    }
    
    public Response setAccepts(List<Map<String, String>> accepts) {
      this.accepts = accepts;
      return this;
    }
    
    public Response setRejects(List<Map<String, String>> rejects) {
      this.rejects = rejects;
      return this;
    }
  }
  

  @Override
  // if LoggedIn
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    String account = request.getParameter("account");
    String password = request.getParameter("password");
    PrintWriter out = response.getWriter();
    Connection conn = null;
    Statement stmt = null;
    try {
      Class.forName("com.mysql.jdbc.Driver");

//      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);

      stmt = conn.createStatement();
      String sql;
      sql = "SELECT password = password(?) as verified FROM secrets where account = ?;";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, password);
      ps.setString(2, account);
      System.out.println(ps);

      Response res = new Response();
      ObjectMapper mapper = new ObjectMapper();
      
      ResultSet rs = ps.executeQuery();
      if (!rs.first()) { // rs.next()
        // empty result
        res.setStatus(Response.Status.NO_USER);
      } else {
        // Retrieve by column name
        boolean verified  = rs.getBoolean("verified");

        if (verified) {
          String sessionid = IdentifierGenerator.getIdentifier();
          sql = "REPLACE sessions VALUES(?, ?, Now());";
          ps = conn.prepareStatement(sql);
          ps.setString(1, account);
          ps.setString(2, sessionid);
          System.out.println(ps);
          ps.execute();
          
          sql = "SELECT invites, accepts, rejects FROM notifications WHERE account=?;";
          ps = conn.prepareStatement(sql);
          ps.setString(1, account);
          System.out.println(ps);
          rs = ps.executeQuery();
          List<Map<String, String>> invites = new ArrayList<Map<String, String>>();
          List<Map<String, String>> accepts = new ArrayList<Map<String, String>>();
          List<Map<String, String>> rejects = new ArrayList<Map<String, String>>();
          
          if (rs.first()) {
            invites = readGroups(conn, rs.getString("invites"));       
            accepts = readGroups(conn, rs.getString("accepts"));       
            rejects = readGroups(conn, rs.getString("rejects"));       
          }
          
          res.setStatus(Response.Status.OK)
            .setAccount(account)
            .setSessionid(sessionid)
            .setInvites(invites)
            .setAccepts(accepts)
            .setRejects(rejects);
          
          // clean notification
          sql = "REPLACE notifications VALUES(?, '', '', '');";
          ps = conn.prepareStatement(sql);
          ps.setString(1, account);
          System.out.println(ps); // TODO DBG
          ps.execute();
        } else {
          res.setStatus(Response.Status.WRONG_PASS);
        }
      }
      out.println(mapper.writeValueAsString(res));
      rs.close();
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }
  
  private List<Map<String, String>> readGroups (Connection conn, String groupList) throws SQLException {
    List<Map<String, String>> res = new ArrayList<Map<String, String>>();
    String[] invitationGroupID = groupList.split(",");
    String sql;
    PreparedStatement ps;
    ResultSet rs;
    for (String groupid: invitationGroupID) {
      sql = "SELECT * from groups where groupid = ? and expiration < now();";
      ps = conn.prepareStatement(sql);
      ps.setString(1, groupid);
      System.out.println(ps);
      rs = ps.executeQuery();
      if (rs.first()) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("groupid", rs.getString("groupid"));
        map.put("name", rs.getString("name"));
        map.put("admin", rs.getString("admin"));
        map.put("members", rs.getString("members"));
        res.add(map);
      }
    }
    return res;
  }
}// end servlet
