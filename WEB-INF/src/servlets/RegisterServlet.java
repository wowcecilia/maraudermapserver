package servlets;
import java.sql.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.IdentifierGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RegisterServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static final String USER = "root";
  static final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  

  @JsonInclude(Include.NON_NULL)
  static class Reponse {
    static public enum Status {OK, DUPLICATE};
    public Status status;
    public String sessionid;
    
    public Reponse setStatus(Status status) {
      this.status = status;
      return this;
    }
    
    public Reponse setSessionId(String sessionid) {
      this.sessionid = sessionid;
      return this;
    }
    
  }
  
  @Override
  // if LoggedIn
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    String account = request.getParameter("account");
    account = account.replaceAll("[-\\s+#%^&*()]", "");
    String password = request.getParameter("password");
    String name = request.getParameter("name");
    PrintWriter out = response.getWriter();
    Connection conn = null;
    Statement stmt = null;
    try {
      Class.forName("com.mysql.jdbc.Driver");

//      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);

      stmt = conn.createStatement();
      String sql;
      sql = "SELECT * FROM Info where account = ?;";
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, account);
      System.out.println(ps);

      ObjectMapper mapper = new ObjectMapper();
      
      ResultSet rs = ps.executeQuery();
      Reponse res = new Reponse();
      if (!rs.first()) { // rs.next()
        sql = "INSERT INTO secrets VALUES (?, password(?))";
        ps = conn.prepareStatement(sql);
        ps.setString(1, account);
        ps.setString(2, password);
        ps.executeUpdate();
        
        sql = "INSERT INTO Info VALUES (?, ?, ?, ?, ?)";
        ps = conn.prepareStatement(sql);
        ps.setString(1, account);
        ps.setString(2, name);
        ps.setString(3, "");
        ps.setString(4, "");
        ps.setString(5,  "");
        ps.executeUpdate();
        
        String sessionid = IdentifierGenerator.getIdentifier();
        
        // empty result
        res.setStatus(Reponse.Status.OK)
          .setSessionId(sessionid);
      } else {
        res.setStatus(Reponse.Status.DUPLICATE);
      }
      out.println(mapper.writeValueAsString(res));
      rs.close();
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }
}// end servlet
