package servlets;
import java.sql.*;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.DeleteGroupServlet.Response.Status;
import util.IdentifierGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;

public class DeleteGroupServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static private final String USER = "root";
  static private final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  
  @JsonInclude(Include.NON_NULL)
  static class Response {
    public enum Status {OK, ERROR, EXPIRED};
    public Status status;
    public Response setStatus(Status s) {
      this.status = s;
      return this;
    }
  }

  @Override
  // if LoggedIn
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    String account = request.getParameter("account");
    String groupid = request.getParameter("groupid");
    String block = request.getParameter("block");
    PrintWriter out = response.getWriter();
    Connection conn = null;
    Statement stmt = null;
    if (account == null || groupid == null) {
      out.write("{\"Status\":\"EXPIRED\"}");
      return;
    }
    try {
      Class.forName("com.mysql.jdbc.Driver");

//      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);
      stmt = conn.createStatement();
      
      String sql;
      PreparedStatement ps;
      ResultSet rs;
      Response res = new Response();
      
      sql = "select * from groups where groupid = ? and expiration > now();";
      ps = conn.prepareStatement(sql);
      ps.setString(1, groupid);
      System.out.println(ps);
      rs = ps.executeQuery();
      if (!rs.next()) {
        res.setStatus(Status.ERROR);
      } else {
        sql = "select coalesce(status, 0) from user_group_privacy where account = ? and groupid = ?";
        ps = conn.prepareStatement(sql);
        ps.setString(1, account);
        ps.setString(2, groupid);
        System.out.println(ps);
        rs = ps.executeQuery();
        int status = rs.next() ? rs.getInt(1) : 0;
        System.out.print(status);
        if (status > 100) { // admin
          // do the work
          sql = "delete from user_group_privacy where groupid = ?";
          ps = conn.prepareStatement(sql);
          ps.setString(1, groupid);
          System.out.println(ps);
          ps.executeUpdate();
          
          sql = "delete from groups where groupid = ?";
          ps = conn.prepareStatement(sql);
          ps.setString(1, groupid);
          System.out.println(ps);
          ps.executeUpdate();
          res.setStatus(Status.OK);
        } else if (status > 0) { 
          // 100: just a member, leave the group
          // 1: delete the invitation
          // 2: delete the request
          int newStatus = (block != null && block.equals("true")) ? -1 : 0;
          sql = "update user_group_privacy set status = ? where groupid = ? and account = ?";
          ps = conn.prepareStatement(sql);
          ps.setInt(1, newStatus);
          ps.setString(2, groupid);
          ps.setString(3, account);
          System.out.println(ps);
          ps.executeUpdate();
          res.setStatus(Status.OK);
        } else {
          res.setStatus(Status.ERROR);
        }
      }
      
      ObjectMapper mapper = new ObjectMapper();
      out.println(mapper.writeValueAsString(res));
      rs.close();
      ps.close();
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }

}// end servlet
