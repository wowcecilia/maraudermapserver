package servlets;
import java.sql.*;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlets.LoginServlet.Response;
import servlets.LoginServlet.Response.Status;
import util.IdentifierGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.*;

public class SearchGroupServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static private final String USER = "root";
  static private final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  
  @JsonInclude(Include.NON_NULL)
  static class Response {
    public enum Status {OK, EXPIRED};
    public Status status;
    public List<String> groupid;
    public List<String> groupnames;
    public List<Integer> relation;
    public Response setStatus(Status s) {
      this.status = s;
      return this;
    }
    
    public Response setGroupid(List<String> a) {
      this.groupid = a;
      return this;
    }
    
    public Response setGroupnames(List<String> a) {
      this.groupnames = a;
      return this;
    }

    public Response setRelation(List<Integer> a) {
      this.relation = a;
      return this;
    }
  }
  

  @Override
  // if LoggedIn
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    String account = request.getParameter("user");
    String keyword = request.getParameter("keyword");
    
    PrintWriter out = response.getWriter();
    Connection conn = null;
    Statement stmt = null;
    List<String> ids = new ArrayList<String>();
    List<String> names = new ArrayList<String>();
    List<Integer> relations = new ArrayList<Integer>();
    try {
      Class.forName("com.mysql.jdbc.Driver");
      conn = DriverManager.getConnection(DB_URL, USER, PASS);

      stmt = conn.createStatement();
      String sql;
      PreparedStatement ps;
      sql = "select b.groupid as id, b.name as name, a.status as relation from" + 
              " (select * from groups where name =?) b" + 
              " left outer join" + 
              " (select * from user_group_privacy where account = ?) a" + 
              " using (groupid) where coalesce(a.status, 0) >= 0;";
      ps = conn.prepareStatement(sql);
      ps.setString(1, keyword);
      ps.setString(2, account);
      System.out.println(ps);
      
      ResultSet rs = ps.executeQuery();
      
      while (rs.next()) {
        ids.add(rs.getString("id"));
        names.add(rs.getString("name"));
        relations.add(rs.getInt("relation"));
      }
      
      sql = "select b.groupid as id, b.name as name, a.status as relation from" + 
          " (select * from groups where name!=? and name regexp ?) b" + 
          " left outer join" + 
          " (select * from user_group_privacy where account = ?) a" + 
          " using (groupid) where coalesce(a.status, 0) >= 0 and (b.expiration > now());";
      
      ps = conn.prepareStatement(sql);
      ps.setString(1, keyword);
      ps.setString(2, keyword);
      ps.setString(3, account);
      System.out.println(ps);
      
      rs = ps.executeQuery();
      
      while (rs.next()) {
        ids.add(rs.getString("id"));
        names.add(rs.getString("name"));
        relations.add(rs.getInt("relation"));
      }

      ObjectMapper mapper = new ObjectMapper();
      Response res = new Response()
        .setStatus(Response.Status.OK)
        .setGroupid(ids)
        .setGroupnames(names)
        .setRelation(relations);
      out.println(mapper.writeValueAsString(res));
      rs.close();
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }

}// end servlet
