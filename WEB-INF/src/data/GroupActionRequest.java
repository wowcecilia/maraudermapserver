package data;

public class GroupActionRequest {
  String from_account;
  String group_id;
  enum ACTION_TYPE {REQUEST_JOIN, REQUEST_ACCEPTED, REQUEST_REJECTED, INVITE_JOIN, INVITATION_REJECTED};
  ACTION_TYPE type;
}
