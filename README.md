Marauder’s Map server design prototype
======================================

Section 1 - Framework
---------------------
* [AWS EC2 instance](http://ec2-54-86-76-107.compute-1.amazonaws.com:8080/alpha)  (temporary a free micro instance)
* Database: MySQL
* Web server: Apache tomcat servlet

Section 2 - Data
----------------
* Messages are in [JSon](http://en.wikipedia.org/wiki/JSON) format. Servlet marshall the raw messages with [Jackson 2.4.3](http://jackson.codehaus.org/).
* Please keep in mind user locations could be very sensitive information. Currently on network messages are encrypted with user-specific temporary session id using **AES** algorithm before we adopt SSL.

The session id was exchanged first time user contacted a server or when there are evidence indicating current session id not safe. Then the following messages will be encrypted with the session id. On server side, databases will keep the session id; on iOS client, the local database is safe enough.

### Protocols:  (Please help me complete revise it so I could integrate into servelet)
#### Register:
```
Request {
  String account; // unique id, maybe phone number
  String name; // username, easy to remember
  String password;
};

Response {
  enum Status {OK, DUPLICATE};
  String sessionid; // only when status is OK
}
```

#### Login (done):
```
Request {
  String account;
  String password;
}

Reponse {
  enum status {OK, WRONG_PASS, NO_USER};
  /* below section exists only when status is ok */
  String sessionid;
  List<Map<String, String>> invites; // a list of groups invitation, each in
}
```

#### GetGroupMap:
```
Request {
  String account;
  String group_id;
  boolean map_requested;
}

Member_location {
  double long;
  double lat;
  long time;
  String account;
}

Response {
  String map_address;
  List<Member_location};
}
```

#### GetGroupInfo:
```
Request {
  String account;
  String group_id;
}

Member_info { // also used in another servelet
  String account;
  String name;
  String profile_pic_address;
}
  
Response {
  String account;
  List<Member_info>;
  String map_address;
  String map_name;
}
  
```

#### Notification:
```
Group_request {
  String from_account;
  String group_id;
  enum type { request, request_accepted, request_rejected, invitation, invitation_rejected};
}

Message {
  String from_account;
  String group_id;
  String message;
  long time;
}

Request {
  String account;
}

Response {
  List<Group_request>;
  List<Message>;
}
```

#### GetContactList:
```
Request {
  List<String> contact_numbers;
}

Response {
  List<MemberInfo> memberInfos; 
}
```

#### GetMemberInfo:
```
Request {
  String account;
}

Response {
  MemberInfo member;
}
```

see [servlet example](http://ec2-54-86-76-107.compute-1.amazonaws.com:8080/alpha/login?account=4128887335&password=abcd)

###Tables: 
#### basic account list: (Info)

|--------|------|-------|-------|-------|

|account | name | image | phone | email |

|--------|------|-------|-------|-------|

```
CREATE table Info (
    account varchar(255) not null,
    name text not null,
    image varchar(255), -- optional
    phone varchar(30), -- optional
    email varchar(255), -- optional
    primary key(account)
    );
```

#### secrets:

|--------|---------------|

|account | md5(password) |

|--------|---------------|

```
create table secrets (
    account text not null, 
    password text not null, 
    primary key(account)
);
```

#### sessions:

|--------|------------|

|account | session_id |

|--------|------------|

```
create table sessions (
    account varchar(255) not null, 
    sessionid BIGINT not null, 
    primary key(account)
);
```

#### group_info:

|----------|---------|--------|-----------------|

| group_id | members | map_id | expiration_date |

|----------|---------|--------|-----------------|

#### Pending Requests:

|------------|---------|--------------|------|-----------------|

| request_id | account | group number | type | expiration_date |

|------------|---------|--------------|------|-----------------|

* type: 0 for invited, 1 for requested (the account should be the account number of the admin), 2 for invitation rejected, 3 for request rejected, 4 for request accepted.
* invitation accepted will notify every member of "someone joined the group"

#### Messages:

|------------|----------|------|---------|------|

| message_id | group_id | from | content | time |

|------------|----------|------|---------|------|

* content could be "joined", from is 0 for system id

#### Notifications:

|---------|--------------------|---------------------|

| account | unread_request_ids | unread_messages_ids |

|---------|--------------------|---------------------|

#### Locations:

|---------|-------|----------|------|

| account | group | location | time |

|---------|-------|----------|------|

## Section 3 - TODO list:
* Use SSL for privacy. You might see that when you login or register you have to send your password and other sensitive information on network. When we got SSL, we could use the public key of server, which is fully available to public, to encrypt the messages, while one can only decrypt the messages with the server’s private key.
* From MySQL switch to HBase (or other distributed database).

## Appendix
### How to use servlet to finish a Database call:

A servlet is a java program running on the server that takes the requeset generate response to the client.
When configured correctly, the response could be generated according to the database.
Our server uses servlet to interact with database.

To write a servlet, you need a class extends HttpServlet.
HttpServelet requires two interface be implemented:

* public void doGet(HttpServletRequest request, HttpServletResponse response)
* public void doPost(HttpServletRequest request, HttpServletResponse response)

The difference of GET and POST lies in how the client send the data to a http server.
As the example code below shows, you can simply forward the post request to get request, or disable GET method.

The below example shows the how to get the username given the telephone number.
Basicly there are three steps:

1. Read the telephone number from the request.
2. Connect to the database and do the sql operation.
3. Write the result to response.

```
import java.sql.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ExampleServlet extends HttpServlet {
  // JDBC driver name and database URL
  static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  static final String DB_URL = "jdbc:mysql://ec2-54-86-76-107.compute-1.amazonaws.com/map";

  // Database credentials
  static private final String USER = "root";
  static private final String PASS = "alphamap";
  private static final long serialVersionUID = 1L;
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    response.setContentType("text/html");   
    
    /* step 1: get the telephone number from request */
    String account = request.getParameter("account");

    /* step 2: connect to the database server */
    Connection conn = null;
    Statement stmt = null;
    try {
      Class.forName("com.mysql.jdbc.Driver"); // to decide how the java program connects to the databse
      conn = DriverManager.getConnection(DB_URL, USER, PASS); // authorization
      stmt = conn.createStatement();

      // the table Info contains the infomation we need. 
      String sql = "SELECT name FROM Info where account = ?;"; 

      // the ? in query will be replaced by the string account we read out from request.
      // the reason we use this function to replace is to prevent sql injection
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, account);

      // read the name from result
      ResultSet rs = ps.executeQuery();
      String name = rs.getString("name");
        
      }

      /* step 3: send the result back to the client */
      PrintWriter out = response.getWriter();
      out.println(mapper.writeValueAsString(res));

      /* finally clean up the connection to databse */
      rs.close();
      stmt.close();
      conn.close();
    } catch (Exception e) {
      // Handle errors for Class.forName
      e.printStackTrace();
    } finally {
      // finally block used to close resources
      try {
        if (stmt != null)
          stmt.close();
      } catch (SQLException se2) {}
      try {
        if (conn != null)
          conn.close();
      } catch (SQLException se) {
        se.printStackTrace();
      }// end finally try
    }// end try
  }// end doGet
  
  @Override
  /* Simply forward the post request and treat the request as GET request */
  public void doPost(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException, ServletException {
      doGet(request, response);
  }
}// end servlet
```

After the code compiled, add the *.class file into the WEB-INF/classes folder.
Also we need to modify WEB-INF/web.xml to map some address to this servelet.
Add the following to web.xml:
```
   <servlet>
        <servlet-name>SomeName</servlet-name>
        <servlet-class>ExampleServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>SomeName</servlet-name>
        <url-pattern>/example</url-pattern>
    </servlet-mapping>
```
After that the servlet will run on website/example.

### How to use Jackson Json package:

* Declare the class you want to marshall into JSON as a class:
```
class ExampleClass {
  String name;
  String email;
}
```
* Use a ObjectMapper to marshall it into string:
```
ExampleClass c = new ExampleClass(name, email);
ObjectMapper mapper = new ObjectMapper();
String marshalled = mapper.writeValueAsString(c);
```

After the code above executed, the value of marshalled:
{"name":"name", "email":"email"}
